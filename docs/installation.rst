============
Installation
============

At the command line either via easy_install or pip::

    $ easy_install scale_exercises
    $ pip install scale_exercises

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv scale_exercises
    $ pip install scale_exercises
