"""
Tests for `scale_exercises` module.
"""
import pytest
from scale_exercises import scale

def test_make_measures():
    meas = scale.make_measures([scale.Note(f"{i}.1") for i in range(1, 5)], 4)
    assert list(meas) == ['D,,E,,F,,G,,',]


def test_mm_fail_with_generator():
    meas = scale.make_measures((scale.Note(f"{i}.1") for i in range(0, 5)), 4)
    with pytest.raises(ValueError):
        a = list(meas)


def test_mm_fail1():
    meas = scale.make_measures([scale.Note(f"{i}.1") for i in range(1, 6)], 4)
    with pytest.raises(ValueError):
        a = list(meas)
