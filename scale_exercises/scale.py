#!/usr/bin/env python3

import argparse
import configparser
import functools
import os.path
import re
import sys

from . import tuneprint

PITCHES = (
    "C,,",
    "D,,",
    "E,,",
    "F,,",
    "G,,",
    "A,,",
    "B,,",
    "C,",
    "D,",
    "E,",
    "F,",
    "G,",
    "A,",
    "B,",
    "C",
    "D",
    "E",
    "F",
    "G",
    "A",
    "B",
    "c",
    "d",
    "e",
    "f",
    "g",
    "a",
    "b",
    "c'",
    "d'",
    "e'",
    "f'",
    "g'",
    "a'",
    "b'",
    "c''",
    "d''",
    "e''",
    "f''",
    "g''",
    "a''",
    "b''",
    "c'''",
    "d'''",
    "e'''",
    "f'''",
    "g'''",
    "a'''",
    "b'''",
)

REV_PITCHES = {n: i for i, n in enumerate(PITCHES)}


class Note:
    """`spec' consists of a relative pitch plus an optional length, of the form 'd' or 'd.d'
  """

    def __init__(self, spec):
        mo = re.search(r"^(\d+)(?:\.(\d+))?", spec)
        assert mo
        self._rel_pitch = int(mo.group(1), 10)
        if mo.group(2):
            self._length = int(mo.group(2), 10)
        else:
            self._length = 1

    @property
    def rel_pitch(self):
        return self._rel_pitch

    @property
    def length(self):
        return self._length

    def __str__(self):
        if self.length == 1:
            return f"{PITCHES[self.rel_pitch]}"
        else:
            return f"{PITCHES[self.rel_pitch]}{self.length}"

    def new_note(self, move):
        newspec = f"{self.rel_pitch + move}.{self.length}."
        return Note(newspec)


def make_measures(notes, beats_per_measure=4, fillin=False):
    measure = []
    beat_count = 0
    i = 0
    for n in notes:
        i += n.length
        measure.append(str(n))
        beat_count += n.length
        if beat_count % beats_per_measure == 0:
            yield "".join(measure)
            measure = []

    if i % beats_per_measure != 0:
        if fillin:
            measure.extend(["z"] * (beats_per_measure - (i % beats_per_measure)))
            yield measure
        else:
            raise ValueError(
                f"Not an integral number of measures in note list (i is {i})"
            )


class Exercise:
    def __init__(self, context, tuneprinter):
        self.context = context
        self.tp = tuneprinter

        self.pattern = [
            Note(p)
            for p in context["pattern"].translate(str.maketrans(",", " ")).split()
        ]
        self.blocks = [
            int(b) for b in context["blocks"].translate(str.maketrans(",", " ")).split()
        ]
        self.partstart = [REV_PITCHES[p] for p in context["parts"].split()]
        self.finish = [
            Note(p)
            for p in context["finish"].translate(str.maketrans(",", " ")).split()
        ]
        self.number = int(context["number"])

    def final_measure(self, part):
        finish_measure = [f.new_note(part) for f in self.finish]
        measure = list(make_measures(finish_measure, 4, fillin=True))
        if measure:
            return "".join(measure[0])
        else:
            return ""

    def printme(self):
        # print(f"pattern: {self.pattern}")
        # print(f"partstart: {self.partstart}")
        # print(f"blocks: {self.blocks}")
        self.tp.prepare()

        measures = []

        for part in self.partstart:
            notes = (
                pattern.new_note(block + part)
                for _ in range(0, int(self.context["repeats"]))
                for block in self.blocks
                for pattern in self.pattern
            )

            measurelist = list(make_measures(notes, 4))
            # print("measurelist:", measurelist)
            tune = list()
            for _ in range(0, int(self.context["repeats"])):
                tune.extend(measurelist)

            # print("final measure is:", self.final_measure(part))
            tune.append(self.final_measure(part))

            self.tp.apply_tune(tune)
            print(f"{'|'.join(tune)}\n")

        print(f"exer({self.context['name']}): {measures}")
        self.tp.finish()

class ScaleExercises:
    def __init__(self, args):
        self.cmd_args = args[1:]

        self.my_args = self.parse_args(self.cmd_args)
        self.config = self.parse_config(self.my_args.config)

    def parse_args(self, cmd_args):
        parser = argparse.ArgumentParser(
            description="Make some scale excersises in ABC."
        )
        parser.add_argument(
            "config", default="scale.cfg", help="Name of exercise config file"
        )
        parser.add_argument(
            "-s",
            "--split-out-parts",
            action="store_true",
            help="Split each part of exercise into separate tune.",
        )
        parser.add_argument(
            "-f",
            "--force",
            action="store_true",
            help="Force creation of new files, even if old ones already exist.",
        )

        return parser.parse_args(cmd_args)

    def parse_config(self, cfgfile):
        config = configparser.ConfigParser()
        config["DEFAULT"] = {
            "key": "A",
            "meter": "4/4",
            "unit": "1/4",
            "tempo": "100",
            "number": "1",
            "parts": "A,",
            "finish": "",
            "reference": "1",
            "output_directory": ".",
            "base_filename": "set.abc",
            "tunesplit": "exercises",
        }
        config.read(cfgfile)

        # Do some validation
        # If the default section has a tune split type of 'book', then the tune split type cannot be changed
        self.ts_is_book = (
            "tunesplit" in config["DEFAULT"]
            and config["DEFAULT"]["tunesplit"] == "book"
        )

        for s in config:
            sect = config[s]
            sect["name"] = s
            # Make sure X: is a number
            if "X" in sect:
                if not re.search(r"^\d+$", sect["X"]):
                    raise ValueError(
                        f"Tune number must be greater than 0, is {sect['X']}"
                    )
                if int(sect["X"]) < 0:
                    raise ValueError(
                        f"Tune number must be greater than 0, is {sect['X']}"
                    )

            if "tunesplit" in sect:
                sect["tunesplit"] = sect["tunesplit"].casefold()
                if not sect["tunesplit"] in ("book", "exercises", "parts"):
                    raise ValueError(
                        "The tune split type must be one of 'book', 'exercises', or 'parts' (is {sect['tunesplit']})"
                    )
                if self.ts_is_book and (sect["tunesplit"] in ("exercises", "parts")):
                    print(
                        f"Warning: Global tune split type is 'book'. It has been set to {sect['tunesplit']} in {s}. Setting back to book"
                    )
                    sect["tunesplit"] = "book"
                if not self.ts_is_book and sect["tunesplit"] == "book":
                    raise ValueError(
                        f"Warning: Global tune split type is not 'book', but it  has been set to 'book' in {s}. A tune type of 'book' must only be set in the default (global) section"
                    )

        return config

    def run(self):
        abc = list()
        if self.ts_is_book:
            tp = tuneprint.TunePrintBookExercise(self.config['DEFAULT'])

        for s in self.config:
            if s == "DEFAULT":
                continue

            print(
                "Config:",
                "\n".join(
                    sorted([f"[{s}][{i}]: {self.config[s][i]}" for i in self.config[s]])
                ),
            )
            if self.ts_is_book:
                tp.apply_context(self.config[s])
            elif self.config[s]['tunesplit'] == 'exercises':
                tp = tuneprint.TunePrintExercise(self.config[s])
            elif self.config[s]['tunesplit'] == 'parts':
                tp = tuneprint.TunePrintParts(self.config[s])
            else:
                raise ValueError("Not ready for whatever")
            exer = Exercise(self.config[s], tp)
            exer.printme()
            tp.print_tunes()
            print("--------------------------------------------------------------")

        if self.ts_is_book:
            tp.epilog()

if __name__ == "__main__":
    ScaleExercises(sys.argv).run()
