

from collections import defaultdict, OrderedDict
import os.path


class TunePrint():
    header_trans = OrderedDict({
        'X': 'reference',
        'T': 'name',
        'C': 'composer',
        'O': 'origin',
        'R': 'rhythm',
        'V': 'voice',
        'M': 'meter',
        'L': 'unit',
        'Q': 'tempo',
        'K': 'key'
        })

    def __init__(self, context):
        self.context = context
        self.root, self.ext = os.path.splitext(self.context["base_filename"])
        self.printer = defaultdict(list)
        self.tunes = list()

    def make_tune_header(self):
        self.hdr = OrderedDict()
        for k, v in TunePrint.header_trans.items():
            if v in self.context:
                self.hdr[k] = self.context[v]

    def apply_tune(self, tune):
        self.tunes.append("|".join(tune))

    def epilog(self):
        pass

class TunePrintExercise(TunePrint):
    def __init__(self, context):
        super().__init__(context)

    def prepare(self):
        self.number = int(self.context['number'])
        self.make_tune_header()
        self.hdr['X'] = f"{self.number}"

    def finish(self):
        filename = f"{self.root}_{self.context['name']}{self.ext}"
        self.printer[filename].append("%abc")
        self.printer[filename].extend([f"{k}: {v}" for k, v in self.hdr.items()])
        self.printer[filename].extend(self.tunes)

    def print_tunes(self):
        for filename in self.printer:
            with open(filename, "w") as fo:
                fo.write("\n".join(self.printer[filename]))
                fo.write("\n")

class TunePrintParts(TunePrint):
    def __init__(self, context):
        super().__init__(context)

    def prepare(self):
        self.number = int(self.context['number'])

    def finish(self):
        part_number = 1
        for tune in self.tunes:
            self.make_tune_header()
            self.hdr['X'] = f"{self.number}"
            filename = f"{self.root}_{self.context['name']}_pt{part_number:02d}{self.ext}"
            self.hdr['T'] += f" Part {part_number}"
            self.printer[filename].append("%abc")
            self.printer[filename].extend([f"{k}: {v}" for k, v in self.hdr.items()])
            self.printer[filename].append(tune)
            part_number += 1

    def print_tunes(self):
        for filename in self.printer:
            with open(filename, "w") as fo:
                fo.write("\n".join(self.printer[filename]))
                fo.write("\n")

class TunePrintBookExercise(TunePrint):
    def __init__(self, context):
        super().__init__(context)
        self.filename = f"{self.root}{self.ext}"
        self.number = int(self.context['number'])

    def apply_context(self, context):
        self.context = context

    def prepare(self):
        self.make_tune_header()
        self.hdr['X'] = f"{self.number}"

    def finish(self):
        self.printer[self.filename].extend([f"{k}: {v}" for k, v in self.hdr.items()])
        self.printer[self.filename].extend(self.tunes)

    def print_tunes(self):
        self.number += 1

    def epilog(self):
        for filename in self.printer:
            with open(filename, "w") as fo:
                fo.write("%abc\n")
                fo.write("\n".join(self.printer[filename]))
                fo.write("\n")
